#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <cstring>

struct User {
	char name[50];
	char age;
};


void pass_test(User* user)
{
	std::cout << "passed: " << (*user).name << " " << user << std::endl;
}

void cpp_struct()
{
	User user;
	strcpy(user.name, "����");
	user.age = 22;

	std::cout << user.name << " " << &user << std::endl;
	pass_test(&user);
}