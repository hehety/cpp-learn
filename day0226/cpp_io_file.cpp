#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;



void write_file()
{
	cout << "write file to D:/cpp.txt" << endl;
	char content[] = "xxxxx";
	ofstream ops;
	ops.open("D:/cpp.txt", ios::trunc);
	ops.write(content, strlen(content));
	ops.flush();
	ops.close();
}

void write_file1(char* content)
{
	cout << "write file to D:/cpp.txt" << endl;
	ofstream ops;
	ops.open("D:/cpp.txt", ios::trunc);
	ops.write(content, strlen(content));
	ops.flush();
	ops.close();
}

void read_file()
{
	ifstream ips("D:/cpp.txt", ios::in);
	if (!ips)
	{
		cerr << "error open file" << endl;
		return;
	}
	char buff[1024];
	size_t len;
	while (!ips.eof())
	{
		ips.read(buff, 1024);
		len = ips.gcount();
		char *tmp = new char[len];
		for (size_t i = 0; i < len; i++)
		{
			tmp[i] = buff[i];
		}
		cout << tmp << endl;
		delete tmp;
	}
	ips.close();
}



void cpp_io_file()
{
	// write_file();
	write_file1("123123");
	read_file();
}