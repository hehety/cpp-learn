#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <iostream>
#include <cstring>


using namespace std;


class Person
{
private:
	unsigned short int height;

public:
	char name[50];
	char sex;
	char age;
	int getAge()
	{
		return this->age;
	};

	void setAge(char age)
	{
		this->age = age;
	};

	// 运算符重载
	Person operator+(Person& man)
	{
		Person m;
		m.age = man.age + this->age;
		return m;
	};


	void eat()
	{
		cout << "person eats foods" << endl;
	};

	// 纯虚函数声明
	// virtual void other() = 0;
	// virtual int other1() = 0;
};

// 有public, protected, private三种继承方式，它们相应地改变了基类成员的访问属性。
// 1.public 继承：基类 public 成员，protected 成员，private 成员的访问属性在派生类中分别变成：public, protected, private
// 2.protected 继承：基类 public 成员，protected 成员，private 成员的访问属性在派生类中分别变成：protected, protected, private
// 3.private 继承：基类 public 成员，protected 成员，private 成员的访问属性在派生类中分别变成：private, private, private
class Man : public Person
{
public:
	Man()
	{
		cout << "a man is born..." << endl;
	};

	void doWorks();// 声明Man的doWorks方法
	bool doWorks(char* workname); // 方法重载
	void eat()
	{
		cout << "man eats foods" << endl;
	};
};

class Woman : protected Person
{
public:
	Woman()
	{
		cout << "a woman is born..." << endl;
	};
	void eat()
	{
		cout << "woman eats foods" << endl;
	};

};

// 实现Man的doWorks方法
void Man::doWorks()
{
	cout << "man do works..." << endl;
}
// 方法重载
bool Man::doWorks(char* workname)
{
	cout << workname << endl;
	cout << "man do "<< workname  << "..." << endl;
	return true;
}




void cpp_object()
{
	Person p;
	strcpy(p.name, "James Band");
	p.setAge(22);
	p.sex = 'F';
	cout << "age of person:" << p.getAge() << endl;

	Man man;
	man.setAge(11);
	man.sex = 'M';
	cout << "man's sex is:" << man.sex << endl;
	man.doWorks();
	char newwork[] = "drive";
	man.doWorks(newwork);
	cout << &newwork << endl;

	man.eat();

	cout << "operation+ of man is:" << (p + man).getAge() << endl;

	Woman wm;
	// wm.sex = 'F'; // cannot access this field.
	
	wm.eat();

}