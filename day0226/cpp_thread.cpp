#include <iostream>
#include <thread>
#include <string>
#ifdef WIN32
#include <Windows.h>
#include <synchapi.h>
#else
#include <unistd.h>
#endif

using namespace std;

void thread_work(string name)
{
	cout << "I'm thread-" << std::this_thread::get_id() << ", my name is " << name << endl;
#ifdef WIN32
	Sleep(3000);
#else
	sleep(3);
#endif // WIN32

	
}




void cpp_thread()
{
	// 创建多线程
	for (size_t i = 0; i < 5; i++)
	{
		string name = "jack-" + std::to_string(i); // string和int拼接
		std::thread(thread_work, name).detach();
		// t.join(); // synchronize threads: 使线程同步执行
	}
	cout << "\nthread create finished\n";
#ifdef WIN32
	Sleep(3000);
#else
	sleep(3);
#endif // WIN32
	cout << "sleep over\n";
}

