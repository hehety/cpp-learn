#include <iostream>
#include <sstream>

using namespace std;


int add1(int a, int b = 20)
{
	return a + b;
}

// 引用调用
void exchange1(int &x, int &y)
{
	int temp = x;
	x = y;
	y = temp;
}

// 指针调用
void exchange2(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void lambdaRec(int x, int y, int callback(int x, int y))
{
	callback(x, y);
}

// Lambda 函数与表达式
void lambda_test1()
{
	lambdaRec(1, 3, [](int x, int y) -> int {
		cout << "lambda callback " << x << ", " << y << endl;
		return 0;
	});
}


void cpp_func()
{
	cout << add1(100, 200) << endl;
	cout << add1(100) << endl;

	int a1 = 11;
	int b1 = 13;
	cout << "a1=" << a1 << ",b1=" << b1 << endl;
	exchange1(a1, b1);
	cout << "a1=" << a1 << ",b1=" << b1 << endl;
	exchange2(&a1, &b1);
	cout << "a1=" << a1 << ",b1=" << b1 << endl;


	lambda_test1();
}





