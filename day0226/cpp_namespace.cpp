#include <iostream>

using namespace std;

namespace man
{
	void call()
	{
		cout << "call man" << endl;
	}
}

namespace woman
{
	void call()
	{
		cout << "call woman" << endl;
	}
}



using namespace woman;

void cpp_namespace()
{
	//man::call();
	//woman::call();
	call();
}



