#include <iostream>

using namespace std;

class User
{
public:
	char *buff;
	User()
	{
		cout << "调用构造函数" << endl;
		buff = new char[102400000];
	}
	~User()
	{
		cout << "调用析构函数" << endl;
		delete buff;
	}
};


// 动态内存
void cpp_dynamic_mem()
{
	char name[10];
	char* buffer = new char[102400000];
	cin >> name;
	cout << "name is " << name << endl;
	cout << "release buffer..." << endl;
	delete buffer;

	// 一维数组的动态内存分配和释放
	cin >> name;
	char* buff1 = NULL;
	buff1 = new char[102400000];
	cin >> name;
	delete [] buff1;

	// 2行3列
	char **buff2 = new char *[2];
	for (size_t i = 0; i < 2; i++)
	{
		buff2[i] = new char[300000000];
	}

	cin >> name;

	cout << "开始释放" << endl;
	for (size_t i = 0; i < 2; i++)
	{
		cin >> name;
		delete[] buff2[i]; // 一维数组的[]可以省略??
	}
	delete[] buff2;
	cin >> name;

	cout << "create user" << endl;
	User *u = new User;
	cin >> name;

	cout << "delete user" << endl;
	delete u;
	cin >> name;

}