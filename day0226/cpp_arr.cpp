#include <iostream>

using namespace std;

// 数组都是由连续的内存位置组成。最低的地址对应第一个元素，最高的地址对应最后一个元素。
void cpp_array()
{
	int a[] = { 3,6,1,8,5,16,27 };
	int *p;
	p = a;

	for (size_t i = 0; i < 7; i++)
	{
		cout << *p << " ";
		p++;
	}

}
